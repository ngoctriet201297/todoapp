﻿using System;
using System.Collections.Generic;

namespace TodoApp.Application.IServices.Todo.Dto
{
    public class GetTodoByPagingOutputDto
    {
        public long TotalItems { get; set; }
        public List<TodoDto> Items { get; set; }
    }
}
