﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TodoApp.Application.IServices.Todo.Dto
{
    public class CreateOrUpdateTodoInputDto
    {
        public Guid? Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? DueDate { get; set; }
        public int DayConstrant { get; set; }
    }
}
