﻿using System;
namespace TodoApp.Application.IServices.Todo.Dto
{
    public class TodoDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public int DayConstrant { get; set; }
        public bool IsExpired
        {
            get
            {
                return DueDate.AddDays(DayConstrant) < DateTime.Now;
            }
        }
    }
}
