﻿using System;
namespace TodoApp.Application.IServices.Todo.Dto
{
    public class GetTodoByPagingInputDto
    {
        public int Take { get; set; }
        public int Skip { get; set; }
    }
}
