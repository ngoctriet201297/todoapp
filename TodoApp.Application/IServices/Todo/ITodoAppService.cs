﻿using System;
using System.Threading.Tasks;
using TodoApp.Application.IServices.Todo.Dto;

namespace TodoApp.Application.IServices.Todo
{
    public interface ITodoAppService
    {
        Task<CreateOrUpdateTodoOutputDto> CreateOrUpdate(CreateOrUpdateTodoInputDto input);
        Task<GetTodoByPagingOutputDto> GetByPaging(GetTodoByPagingInputDto input);
        Task Delete(Guid id);
        Task<TodoDto> GetById(Guid id);
    }
}
