﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TodoApp.Application.IServices.Todo;
using TodoApp.Application.IServices.Todo.Dto;
using TodoApp.EntityFramework.Entities;
using TodoApp.EntityFramework.Repositories;

namespace TodoApp.Application.Services.Todos
{
    public class TodoAppService : ITodoAppService
    {
        private readonly IRepository<Todo> _repository;
        private readonly Mapper _mapper;

        public TodoAppService(IRepository<Todo> repository)
        {
            _repository = repository;
            _mapper = TodoAutoMapper.InitializeAutomapper();
        }

        public async Task<CreateOrUpdateTodoOutputDto> CreateOrUpdate(CreateOrUpdateTodoInputDto input)
        {
            var entity = _mapper.Map<Todo>(input);
            await _repository.Save(entity);
            return _mapper.Map<CreateOrUpdateTodoOutputDto>(entity);
        }

        public Task Delete(Guid id)
        {
            return _repository.Delete(id);
        }

        public async Task<TodoDto> GetById(Guid id)
        {
            var entity = await _repository.GetById(id);
            return _mapper.Map<TodoDto>(entity);
        }

        public async Task<GetTodoByPagingOutputDto> GetByPaging(GetTodoByPagingInputDto input)
        {
            var resp = new GetTodoByPagingOutputDto();
            var query = _repository.GetDbSet();
            var todos = await query.Take(input.Take).Skip(input.Skip).OrderBy(x => x.DueDate).ToListAsync();
            var totalItems = await query.LongCountAsync();
            resp.TotalItems = totalItems;
            resp.Items = _mapper.Map<List<TodoDto>>(todos);
            return resp;
        }
    }
}
