﻿using System;
using AutoMapper;
using TodoApp.Application.IServices.Todo.Dto;
using TodoApp.EntityFramework.Entities;

namespace TodoApp.Application.Services.Todos
{
    public class TodoAutoMapper
    {
        public static Mapper InitializeAutomapper()
        {
            //Provide all the Mapping Configuration
            var config = new MapperConfiguration(cfg =>
            {
                
                cfg.CreateMap<CreateOrUpdateTodoInputDto, Todo>();
                cfg.CreateMap<Todo, CreateOrUpdateTodoOutputDto>();
                cfg.CreateMap<Todo, TodoDto>();

            });
            //Create an Instance of Mapper and return that Instance
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
