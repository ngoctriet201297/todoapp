﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TodoApp.EntityFramework.Entities;

namespace TodoApp.EntityFramework.Database
{
    public class ToDoDbContext : DbContext
    {
        public DbSet<Todo> Todos { get; set; }
        public ToDoDbContext(DbContextOptions<ToDoDbContext> options) : base(options) { }

    }
}
