﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TodoApp.EntityFramework.Entities
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
