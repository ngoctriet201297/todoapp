﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TodoApp.EntityFramework.Entities
{
    public class Todo : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
        public int DayConstrant { get; set; }
    }

}
