﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TodoApp.EntityFramework.Entities;

namespace TodoApp.EntityFramework.Repositories
{
    public interface IRepository<Entity> where Entity : BaseEntity
    {
        Task Save(Entity entity);
        Task<Entity> GetById(Guid id);
        Task Delete(Guid id);
        DbSet<Entity> GetDbSet();
    }
}
