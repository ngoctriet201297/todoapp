﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TodoApp.EntityFramework.Database;
using TodoApp.EntityFramework.Entities;

namespace TodoApp.EntityFramework.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly ToDoDbContext _dbContext;

        public Repository(ToDoDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Delete(Guid id)
        {
            var entity = await _dbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public Task<TEntity> GetById(Guid id)
        {
            return _dbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public DbSet<TEntity> GetDbSet()
        {
            return _dbContext.Set<TEntity>();
        }

        public async Task Save(TEntity input)
        {
            var isExist = await _dbContext.Set<TEntity>().AnyAsync(x => x.Id == input.Id);
            if(isExist)
            {
                _dbContext.Set<TEntity>().Update(input);
            }
            else
            {
                await _dbContext.Set<TEntity>().AddAsync(input);
            }
            await _dbContext.SaveChangesAsync();
        }
    }
}
