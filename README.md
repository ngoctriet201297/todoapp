# TODO Application

## How to configure and run the application
1. Go to ``TodoApp/appsettings.json`` file
2. Update connection string SQL Server
```json
 "ConnectionStrings": {
    "DefaultConnection": "Server=mssql-117757-0.cloudclusters.net,10051;Database=todo;User Id=admin;Password=DISy3f6X;"
  }
```
3. Migrate database, go to ``TodoApp.EntityFramework`` folder and run ``dotnet ef database update -s ../TodoApp``
4. Run the application, go to ``TodoApp`` folder and run ``dotnet run``

## How to test the application
1. Create a todo
``` cURL
curl --location --request POST 'localhost:5000/todo/api' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Learn english",
    "description": "3 hours per day",
    "dueDate": "2023-07-16T19:04:05.7257911-06:00",
    "dayConstrant": 3
}'
```

2. Update a todo 
``` cURL
curl --location --request PUT 'localhost:5000/todo/api' \
--header 'Content-Type: application/json' \
--data-raw '{
     "id": "7074c5b3-44ef-4bfb-a6c1-08db2e186f63",
    "title": "Learn C#",
    "description": "How to integrate auth",
    "dueDate": "2023-07-16T19:04:05.7257911-06:00",
    "dayConstrant": 3
}'
```

3. Get by paging
``` cURL
curl --location --request GET 'localhost:5000/todo/api/get-by-paging?take=1&skip=0' \
--data-raw ''
```

4. Get by id
``` cURL
curl --location --request GET 'localhost:5000/todo/api?id=7074c5b3-44ef-4bfb-a6c1-08db2e186f63' \
--data-raw ''
```

5. Delete by id
``` cURL
curl --location --request DELETE 'localhost:5000/todo/api?id=7074c5b3-44ef-4bfb-a6c1-08db2e186f63' \
--data-raw ''
```