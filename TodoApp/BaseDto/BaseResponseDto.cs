﻿using System;
using System.Collections.Generic;

namespace TodoApp.BaseDto
{
    public class BaseResponseDto<TData>
    {
        public List<string> Errors { get; set; }
        public TData Data { get; set; }
    }
}
