﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApp.Application.IServices.Todo;
using TodoApp.Application.IServices.Todo.Dto;
using TodoApp.BaseDto;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TodoApp.Controllers
{
    [ApiController]
    [Route("[controller]/api")]
    public class TodoController : Controller
    {
        private readonly ITodoAppService _todoAppService;

        public TodoController(ITodoAppService todoAppService)
        {
            _todoAppService = todoAppService;
        }

        [HttpPost]
        public Task<BaseResponseDto<CreateOrUpdateTodoOutputDto>> Create([FromBody]CreateOrUpdateTodoInputDto input)
        {
            return CreateOrUpdate(input);
        }

        [HttpPut]
        public Task<BaseResponseDto<CreateOrUpdateTodoOutputDto>> Update([FromBody] CreateOrUpdateTodoInputDto input)
        {
            return CreateOrUpdate(input);
        }

        [HttpDelete]
        public async Task<BaseResponseDto<object>> Delete(Guid id)
        {
            var resp = new BaseResponseDto<object>()
            {
                Errors = new List<string>()
            };
            try
            {
                await _todoAppService.Delete(id);
            }
            catch(Exception ex)
            {
                resp.Errors.Add(ex.Message);
            }
            return resp;
        }

        [HttpGet]
        public async Task<BaseResponseDto<TodoDto>> Get(Guid id)
        {
            var resp = new BaseResponseDto<TodoDto>()
            {
                Errors = new List<string>()
            };
            try
            {
                resp.Data = await _todoAppService.GetById(id);
            }
            catch (Exception ex)
            {
                resp.Errors.Add(ex.Message);
            }
            return resp;
        }

        [HttpGet("get-by-paging")]
        public async Task<BaseResponseDto<GetTodoByPagingOutputDto>> GetByPaging(int take, int skip)
        {
            var resp = new BaseResponseDto<GetTodoByPagingOutputDto>()
            {
                Errors = new List<string>()
            };
            try
            {
                resp.Data = await _todoAppService.GetByPaging(new GetTodoByPagingInputDto
                {
                    Take = take,
                    Skip = skip
                });
            }
            catch (Exception ex)
            {
                resp.Errors.Add(ex.Message);
            }
            return resp;
        }

        #region Private
        public async Task<BaseResponseDto<CreateOrUpdateTodoOutputDto>> CreateOrUpdate(CreateOrUpdateTodoInputDto input)
        {
            var resp = new BaseResponseDto<CreateOrUpdateTodoOutputDto>
            {
                Errors = new List<string>()
            };
            if (!input.DueDate.HasValue)
            {
                resp.Errors.Add("DueDate should be not null");
                return resp;
            }
            if (input.DueDate <= DateTime.Now)
            {
                resp.Errors.Add("Due Date shouldn't be the past");
                return resp;
            }
            try
            {
                var entityDto = await _todoAppService.CreateOrUpdate(input);
                resp.Data = entityDto;
            }
            catch (Exception ex)
            {
                resp.Errors.Add(ex.Message);
            }
            return resp;
        }
        #endregion
    }
}
